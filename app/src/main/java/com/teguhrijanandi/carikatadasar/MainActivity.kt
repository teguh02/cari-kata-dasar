package com.teguhrijanandi.carikatadasar

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        if(!isNetworkAvailable(applicationContext)) {
            Toast.makeText(applicationContext, "Aplikasi ini membutuhkan koneksi internet!", Toast.LENGTH_SHORT).show()
        }

        submitData();
    }

    fun submitData() {
        val button = findViewById<Button>(R.id.submitBtn)
        button.setOnClickListener {

            if(isNetworkAvailable(applicationContext)) {
                var inputanTeks = findViewById(R.id.inputanTeks) as EditText

                val intent = Intent(this@MainActivity, hasil::class.java)
                intent.putExtra("kalimat",inputanTeks.text.toString())
                startActivity(intent)
            } else {
                Toast.makeText(applicationContext, "Aplikasi ini membutuhkan koneksi internet!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun isNetworkAvailable(context: Context?): Boolean {
        if (context == null) return false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        return true
                    }
                }
            }
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            }
        }
        return false
    }
}