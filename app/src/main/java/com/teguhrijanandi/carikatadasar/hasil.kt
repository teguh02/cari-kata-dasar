package com.teguhrijanandi.carikatadasar

import android.content.ClipData
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.ClipboardManager
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import okhttp3.*
import org.json.JSONObject
import java.net.URLEncoder

class hasil : AppCompatActivity() {

    var kalimat = "";
    private val client = OkHttpClient()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hasil)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        this.kalimat = intent.getStringExtra("kalimat").toString()

        Log.i("inputTeks", this.kalimat);

        stemmingProcess();
        copyBtn();
    }

    fun stemmingProcess() {
        // Set kalimat riwayat
        val textView = findViewById<TextView>(R.id.teksRiwayat)
        textView.text = "Kalimat '" + this.kalimat + "' memiliki kata dasar sebagai berikut :"

        val request = Request.Builder()
            .url("https://teguh.rijanandi.com/stemming_php/?apikey=TeguhRijanandiStemmingWord123%21&kalimat=" + URLEncoder.encode(this.kalimat, "utf-8"))
            .build()

        client.newCall(request).execute().use { response ->
            //if (!response.isSuccessful) throw IOException("Unexpected code $response")

            if (!response.isSuccessful) {
                Toast.makeText(applicationContext, "Unexpected code : $response", Toast.LENGTH_LONG).show();
            }

            var jsonString = response.body!!.string();

            val json = JSONObject(jsonString)
            val results: String = json.getString("results")

            // Set kalimat hasil
            val textHasil = findViewById<TextView>(R.id.teksHasil)
            textHasil.text = results
        }
    }

    fun copyBtn() {
        val button = findViewById<Button>(R.id.copyBtn)
        button.setOnClickListener {
            val textViewHasil = findViewById<TextView>(R.id.teksHasil).text.toString()
            setClipboard(context = this, textViewHasil);

            Toast.makeText(applicationContext, "Teks berhasil disalin", Toast.LENGTH_SHORT).show();
        }
    }

    private fun setClipboard(context: Context, text: String) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.text = text
        } else {
            val clipboard =
                context.getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clip = ClipData.newPlainText("Copied Text", text)
            clipboard.setPrimaryClip(clip)
        }
    }
}